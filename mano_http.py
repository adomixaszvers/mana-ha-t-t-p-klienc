import socket
import urllib
import re

class HttpResponse:
    pass

def recvall(socketobj, packet_size=1024):
    data = []
    while True:
        packet = socketobj.recv(packet_size)
        if packet == b'':
            break
        data.append(packet)
    result = b''.join(data)
    return result


def get(url):
    parsed_url = urllib.parse.urlparse(url)
    request = "GET {path}?{query}\r\nHost: {netloc}\r\n".format(path=parsed_url.path,
               query=parsed_url.query,
               netloc=parsed_url.netloc)
    s = socket.socket()
    #s.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
    if parsed_url.port is not None:
        port = parsed_url.port
    else:
        port = 80
    # print(parsed_url.netloc.encode())
    # print(port)
    s.connect((parsed_url.hostname.encode(), port))
    s.sendall(request.encode())
    data = recvall(s)
    s.close()
    return data


def get2(url):
    data = get(url)
    location_list = re.findall(b'Location: (\S+)', data)
    if len(location_list) != 0:
        data = get2(location_list[0].decode())
    return data

def post(url, forms):
    parsed_url = urllib.parse.urlparse(url)
    encoded_forms = ''
    for key, value in forms.items():
        encoded_forms += '{key}: {value}\r\n'.format(key=key, value=value)
    request="POST {path}\r\nHost: {netloc}\r\nRequest-URI: {path}\r\nContent-Length: 0\r\n{form_data}".format(path=parsed_url.path,
                            query=parsed_url.query,
                            netloc=parsed_url.netloc,
                            form_data=encoded_forms)
    print(request.encode())
    s = socket.socket()
    if parsed_url.port is not None:
        port = parsed_url.port
    else:
        port = 80
    # print(parsed_url.netloc.encode())
    # print(port)
    s.connect((parsed_url.hostname.encode(), port))
    s.sendall(request.encode())
    return recvall(s)


def httpdecode(data):
    r = re.compile(b'((HTTP/(?P<version>1.[01]) (?P<status>\d{3}) (?P<status_message>[\w\S ]+)\r\n)(?P<header>([\w-]+: [\w\S ]+\r\n)*)\r\n)?(?P<body>.*)', flags=re.DOTALL)
    search = r.search(data)
    http_response = HttpResponse()
    http_response.data = data
    http_response.version = search.group('version')
    if search.group('status') is not None:
        http_response.status = int(search.group('status'))
    else:
        http_response.status = None
    http_response.status_message = search.group('status_message')
    http_response.header = dict()
    if search.group('header') is not None:
        r = re.compile(b'(?P<header>[\w-]+): (?P<value>[\w\S ]+)')
        for line in search.group('header').splitlines():
            header_search = r.search(line)
            h = header_search.group('header')
            v = header_search.group('value')
            http_response.header[h.upper()] = v
    http_response.body = search.group('body')
    return http_response

class HttpRequest:
    def __init__(self, method, url, body, headers={}):
        self.method = method
        self.parsed_url = urllib.parse.urlparse(url)
        self.body = body
        self.headers = headers

    def __str__(self):
        string = ''
        string += '{method} {path} HTTP/1.0'.format(method=self.method, path=self.parsed_url.path)
        if self.parsed_url.query != '':
            string +=self.parsed_url.query
        string += '\r\n';
        if self.body is not None:
            self.headers['CONTENT-LENGTH'] = len(self.body)
        else:
            self.headers['CONTENT-LENGTH'] = 0
        if self.parsed_url.netloc is not None:
            self.headers['HOST'] = self.parsed_url.netloc
        for key, value in self.headers.items():
            string += '{key}: {value}\r\n'.format(key=key, value=value)
        string += '\r\n';
        if self.body is not None:
            string += self.body
        return string

def magic(http_request):
    parsed_url = http_request.parsed_url
    request = str(http_request)
    s = socket.socket()
    if parsed_url.port is not None:
        port = parsed_url.port
    else:
        port = 80
    # print(parsed_url.netloc.encode())
    # print(port)
    s.connect((parsed_url.hostname.encode(), port))
    s.sendall(request.encode())
    data = b''
    while True:
        packet = s.recv(1024)
        print('Received packet\n')
        print(packet)
        if packet == b'':
            break
        response = httpdecode(data)
        # if response.header.get('TRANSFER-ENCODING') = 'chunked':
            # TODO
        data += packet
        if response.header.get('CONTENT-LENGTH') is not None:
            if len(response.body) >= response.header.get('CONTENT-LENGTH'):
                break

    return httpdecode(data)

def test():
    request = HttpRequest('POST', 'http://duckduckgo.com/', 'q=Jonas', headers={'Content-Type': 'application/x-www-form-urlencoded'})
    response = magic(request)
    return (request, response)


if __name__ == "__main__":
    request, response = test()
    f = open('/tmp/puslapis.html', 'wb')
    f.write(response.body)
    f.close()
    request = HttpRequest('GET', 'http://www.vu.lt/site_files/InfS/vaizdai_spaudai/sp_VU_zenklas.svg', '')
    response = magic(request)
    f = open('/tmp/vulogo.svg', 'wb')
    f.write(response.body)
    f.close()

